.PHONY: all test clean

build:
	npm run build
	npm run build:types

publish:
	./publish.sh

clean:
	rm -rf lib
	rm -rf coverage
	rm -f junit.xml

test:
	npm run lint
	npm run test

release: clean test build publish
