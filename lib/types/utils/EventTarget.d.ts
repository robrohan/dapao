export declare class EventTarget {
    _listeners: any;
    addEventListener(type: string, listener: (event: any) => void): this;
    hasEventListener(type: string, listener: (event: any) => void): boolean;
    hasAnyEventListener(type: string): boolean;
    removeEventListener(type: string, listener: (event: any) => void): this;
    dispatchEvent(event: any): EventTarget;
}
