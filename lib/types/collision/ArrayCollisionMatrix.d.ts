import { Body } from '../objects/Body';
export declare class ArrayCollisionMatrix {
    matrix: number[];
    constructor();
    get(ii: Body, ji: Body): number;
    set(ii: Body, ji: Body, value: boolean): void;
    reset(): void;
    setNumObjects(n: number): void;
}
